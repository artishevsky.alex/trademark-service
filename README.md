# Trademark FTP server parser with FastAPI interface

In the developed world, businesses can protect their brands by trademark registration.
During such a process, the user needs to check whether their trademark is not already taken by someone
else. This is roughly similar to how domain registration works.

This application is a validation service operating on top of a given trademark database.

Trademark database server it is an FTP server with .zip files inside.
These files should contain .xml files in any inner folder structure.
Required structure for .xml file with a trademark:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Transaction>
    <TradeMarkTransactionBody>
        <TransactionContentDetails>
            <TransactionData>
                <TradeMarkDetails>
                    <TradeMark>
                    <MarkFeature>Figurative</MarkFeature>
                    <WordMarkSpecification>
                        <MarkVerbalElementText>Hongtoo</MarkVerbalElementText>
                    </WordMarkSpecification>
                    </TradeMark>
                </TradeMarkDetails>
            </TransactionData>
        </TransactionContentDetails>
    </TradeMarkTransactionBody>
</Transaction>
```

Here you can perform two types of queries: 
- search trademark by name. Query example:
```
{
  tradeMark(tradeMarkName: "SoloTravelHostel") {
    Id
    TradeMarkTransactionBody {
      TransactionContentDetails {
        TransactionData {
          TradeMarkDetails {
            TradeMark {
              MarkFeature
              WordMarkSpecification {
                MarkVerbalElementText
              }
            }
          }
        }
      }
    }
  }
}
```
- search trademark by similar name. Query example:
```
{
  tradeMarkSimilarity(tradeMarkName: "Data", limit: 10) {
    score
    Id
    TradeMarkTransactionBody {
      TransactionContentDetails {
        TransactionData {
          TradeMarkDetails {
            TradeMark {
              MarkFeature
              WordMarkSpecification {
                MarkVerbalElementText
              }
            }
          }
        }
      }
    }
  }
}
```



## Prerequisites
* Python 3.9+
* Pipenv
* Docker
* Docker-compose
## Installation
### Requirements
* Pipenv
```shell script
pip install --upgrade pip
pip install pipenv
```

## How to run
### Local

For local development you need to define in the .env file
define environmental variables for you running MongoDB server
```shell script
cd trade_mark_service
pipenv install
uvicorn src.main:app --reload --host 0.0.0.0
```

### Docker-compose

In the docker-compose you already have MongoDB service to run.
So, simply use this command to create your docker containers and run them

```shell script
cd trade_mark_service
docker-compose up --build
```

## Config examples

### .env
```ini
MONGO_HOST=<mongodb_host_url>
MONGO_PORT=<mongodb_port>
MONGO_INITDB_ROOT_USERNAME=<mongodb_username>
MONGO_INITDB_ROOT_PASSWORD=<mongodb_password>
MONGO_INITDB_DATABASE=<mongodb_base_database_name>

TRADE_MARK_COLLECTION=<mongodb_trade_mark_collection_name>
PARSED_FILE_COLLECTION=<mongodb_parsed_file_collection_name>

FTP_HOST=<ftp_server_host_url>
FTP_USERNAME=<ftp_server_username>
FTP_PASSWORD=<ftp_server_password>
FTP_ROOT_FOLDER=<ftp_server_root_folder>
```

## Application modules and files description

### src.lib.ftp

Module to deal with FTP server with trademarks.
It could be moved into another application to provide trademark parsing functionality in your application.

This module has next third-party dependencies: 
- xmltodict - library to parse .xml files into python dict structure.

In the file `ftp_client.py` contains context manager to interact with FTP connection.

`ftp_service.py` defines a few functions to get all files from provided folder on the FTP server 
and also to deal with .zip files data.

`async_ftp_service.py` - it is an async layer on `ftp_service.py` to give a capability to run functions in an asynchronous way.

Main logic of trademark parsing is located it the file `ftp_parser.py`.
To use this package you need to simply run function `parse_ftp_server.py` from this file and provide next arguments:

```python
ftp_host: str
ftp_username: str
ftp_password: str
ftp_root_folder: str
save_trade_mark_method: Callable[[dict], Awaitable[Any]]
is_file_parsed_method: Callable[[str], Awaitable[bool]]
save_parsed_file_method: Callable[[str], Awaitable[Any]]
update_parsed_file_method: Callable[[str], Awaitable[Any]]
```

### src.core

Contains settings file for an application and also define file with next utils:
- `get_trade_mark_field_name` - Returns path to trade mark name field
- `create_dataclass_with_non_lists_fields` - Creates dataclass with provided data and skips non-listed in class fields
- `create_subclasses_in_dict` - Recursively creates classes from dict data.
        Gets class name by dict key and creates instance of it with fields data from dict value

### src.db

This module defines all database connectivity.
In the `__init__.py` file you can find database manager variable definition.

To integrate with database different from MongoDB,
you could write custom manager that should define all attributes and methods from abstract class `AsyncBaseManager`.
By default, there is one manager to define application functionality using MongoDB database.
It stores in the file `async_mongo_manager.py`.

### src.graphql_schemas

Here you have schema to query trademark functionality:
- get trademark data by name
- get trademarks data by similar name

Also in this file defined GraphQL schema types for trademark data structure.

### src.routers

`trade_mark_router.py` - file to encapsulate GraphQL trademark router definition.

In the file `__init__.py` there is one router that will include other routers from the application.

### src.dependencies

Module to store all FastAPI dependencies that can be used with build in FastAPI class `Depends`.

File `get_graphql_context.py` has `get_graphql_context` function to provide in a GraphQL resolver
database client object in a context dict.

### src.main

This is a file with the FastAPI application creation.

It has the application definition with included routers from `src.routers.__inti__.py`.
Also, FastAPI server has `startup` function with calls to create database connection, indexes
and also to remove all not finished parsing files from the database.

One more function that located in this file is `shutdown` with database connection closing.

In this file you can find definition of job creation for scheduler that will run with `parse_ftp_server` function call.
This function from the module `src.lib.ftp`.

